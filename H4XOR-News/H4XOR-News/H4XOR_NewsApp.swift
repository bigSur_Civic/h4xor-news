//
//  H4XOR_NewsApp.swift
//  H4XOR-News
//
//  Created by Bin on 19/04/2022.
//

import SwiftUI

@main
struct H4XOR_NewsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
