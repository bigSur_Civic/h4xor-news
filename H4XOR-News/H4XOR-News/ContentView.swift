//
//  ContentView.swift
//  H4XOR-News
//
//  Created by Bin on 19/04/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            List(posts) { post in
                Text(post.title)
            }
            .navigationTitle("H4XOR NEWS")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//struct Post: Identifiable {
//    let id: String
//    let title: String
//}
let posts = [
Post(objectID: "1", points: 2, title: "Hello", url: "someUrl"),
Post(objectID: "2", points: 4, title: "Hola", url: "anotherUrl")
]
